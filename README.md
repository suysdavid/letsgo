WheelOfFortune 
==============
Een Symfony2 applicatie gemaakt door David Suys in opdracht van WimiOnline.be

Installatie
-----------
Sources binnenhalen

    git clone <https-adress-repo> <target-dir>
    
Controleren of de server aan alle requirements voldoet, en fixen waar nodig: 

    php app/check.php

Vendor-packages ophalen met composer, en parameters instellen: 

    composer install --no-dev --optimize-autoloader

Indien DB uit parameters.yml nog niet bestond, deze maken: 

    php app/console doctrine:schema:create --env=prod

data importeren via fixtures: 

    php app/console doctrine:fixtures:load -n --env=prod

cache clearen: 

    php app/console cache:clear --env=prod --no-debug