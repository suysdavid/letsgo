<?php

namespace TestDavid\WheelOfFortuneBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use TestDavid\WheelOfFortuneBundle\Entity\Player;
use TestDavid\WheelOfFortuneBundle\Form\RegisterPlayer;

class WheelOfFortuneController extends Controller {

    /**
     * @Route("/", name="index")
     * 
     * Default: toon registratieformulier
     * Indien form verstuurd geweest is: valideer, bepaal de Prize 
     * en voeg Player toe aan database
     * @return Response
     */
    public function indexAction(Request $request) {
        $player = new Player();
        $form = $this->get('form.factory')->create(new RegisterPlayer(), $player);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->get('doctrine')->getEntityManager();
                $player->setPrize($this->spinTheWheel());
                $em->persist($player);
                $em->flush();
                $request->getSession()->set('player', $player);
                return $this->redirect($this->generateUrl('play', ['playerid'=>$player->getId()]));
            }
        }

        return $this->render('WheelOfFortuneBundle:WheelOfFortune:index.html.twig', array(
                    'form' => $form->createView(),
                    'invalid' => $form->isSubmitted() && !$form->isValid(),
                    'prizes' => $this->getAllPrizes()
        ));
    }

    /**
     * @Route("/play/{playerid}/{showprize}", name="play")
     * 
     * @param string $playerid ID van de speler
     * @param bool $showprize Toon prijs in HTML, in geval van geen JS
     * @return Response
     */
    public function playAction($playerid, $showprize = false) {
        $player = $this->getDoctrine()
                ->getRepository('WheelOfFortuneBundle:Player')
                ->find($playerid);
        if (!$player) {
            return $this->redirect($this->generateUrl('index'));
        }

        return $this->render('WheelOfFortuneBundle:WheelOfFortune:play.html.twig', array(
                    'player' => $player,
                    'showprize' => $showprize
        ));
    }

    /**
     * @Route("/spin/{playerid}", name="spin")
     * 
     * @param string $playerid ID van de speler
     * @return Response
     * @throws \RuntimeException
     */
    public function spinAction(Request $request, $playerid) {
        $player = $this->getDoctrine()
                ->getRepository('WheelOfFortuneBundle:Player')
                ->find($playerid);
        if (!$player) {
            throw new \RuntimeException('No active player');
        }
        if ($request->isXmlHttpRequest()) {
            return $this->render('WheelOfFortuneBundle:WheelOfFortune:spin.html.twig', array(
                        "prize" => $player->getPrize()
            ));
        } else {
            return $this->redirect($this->generateUrl('play', ['playerid'=>$player->getId(), 'showprize' => true]));
        }
    }

    /**
     * Alle prijzen ophalen, aflopend geordend op odds
     * 
     * @return array \TestDavid\WheelOfFortuneBundle\Entity\Prize
     */
    private function getAllPrizes() {
        return $this->getDoctrine()->getRepository('WheelOfFortuneBundle:Prize')->findBy([], ['odds' => 'DESC']);
    }

    /**
     * Een willekeurige (en beschikbare) prijs ophalen
     * 
     * @return \TestDavid\WheelOfFortuneBundle\Entity\Prize|null
     */
    private function spinTheWheel() {
        $prizes = $this->getAllPrizes();
        $random = mt_rand() / mt_getrandmax();

        $spinned = null;
        foreach ($prizes as $prize) {
            $random -= $prize->getOdds() / 100;
            if ($random <= 0) {
                $spinned = $prize;
                break;
            }
        }
        if ($spinned && $spinned->getMaxWinners() > count($spinned->getWinners())) {
            return $spinned;
        }
        return;
    }

}
