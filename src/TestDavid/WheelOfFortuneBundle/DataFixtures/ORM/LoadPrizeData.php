<?php

namespace TestDavid\WheelOfFortuneBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TestDavid\WheelOfFortuneBundle\Entity\Prize;

class LoadPrizeData implements FixtureInterface {

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager) {
        $prizes = [
            [
                "description" => "voucher van 10 euro op napoleongames.be",
                "maxWinners" => 1000,
                "odds" => 50
            ], [
                "description" => "voucher van 100 euro op napoleongames.be",
                "maxWinners" => 100,
                "odds" => 20
            ], [
                "description" => "voucher van 200 euro op napoleongames.be",
                "maxWinners" => 10,
                "odds" => 5
            ], [
                "description" => "reis naar Las Vegas",
                "maxWinners" => 1,
                "odds" => 0.5
            ],
        ];

        foreach ($prizes as $prizedata) {
            $prize = new Prize();
            $prize->setDescription($prizedata['description'])
                    ->setMaxWinners($prizedata['maxWinners'])
                    ->setOdds($prizedata['odds']);
            $manager->persist($prize);
        }

        $manager->flush();
    }

}
