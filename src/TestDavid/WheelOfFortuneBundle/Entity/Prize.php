<?php

namespace TestDavid\WheelOfFortuneBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prize
 *
 * @ORM\Table(name="wof_prize")
 * @ORM\Entity
 */
class Prize
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="maxWinners", type="integer")
     */
    private $maxWinners;

    /**
     * @var float
     *
     * @ORM\Column(name="odds", type="float")
     */
    private $odds;
    
    /**
     * @ORM\OneToMany(targetEntity="Player",mappedBy="prize")
     * @ORM\OrderBy({"registeredAt" = "ASC"})
     */
    private $winners;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Prize
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set maxWinners
     *
     * @param integer $maxWinners
     * @return Prize
     */
    public function setMaxWinners($maxWinners)
    {
        $this->maxWinners = $maxWinners;

        return $this;
    }

    /**
     * Get maxWinners
     *
     * @return integer 
     */
    public function getMaxWinners()
    {
        return $this->maxWinners;
    }

    /**
     * Set odds
     *
     * @param float $odds
     * @return Prize
     */
    public function setOdds($odds)
    {
        $this->odds = $odds;

        return $this;
    }

    /**
     * Get odds
     *
     * @return float 
     */
    public function getOdds()
    {
        return $this->odds;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->winners = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get winners
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWinners()
    {
        return $this->winners;
    }
}
