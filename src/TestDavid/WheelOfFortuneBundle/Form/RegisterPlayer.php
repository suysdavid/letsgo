<?php

namespace TestDavid\WheelOfFortuneBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use libphonenumber\PhoneNumberFormat;

class RegisterPlayer extends AbstractType {

    /**
     * Builds the RegisterPlayer form
     * @param  \Symfony\Component\Form\FormBuilderInterface $builder
     * @param  array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('firstName')
                ->add('lastName')
                ->add('email')
                ->add('address')
                ->add('postcode')
                ->add('city')
                ->add('phoneNumber', 'tel', array('default_region' => 'BE', 'format' => PhoneNumberFormat::NATIONAL))
                ->add('submit', 'submit');
    }

    /**
     * Returns the default options/class for this form.
     * @param array $options
     * @return array The default options
     */
    public function getDefaultOptions(array $options) {
        return array(
            'data_class' => 'TestDavid\WheelOfFortuneBundle\Entity\Player'
        );
    }

    /**
     * Mandatory in Symfony2
     * Gets the unique name of this form.
     * @return string
     */
    public function getName() {
        return 'register_player';
    }

}
