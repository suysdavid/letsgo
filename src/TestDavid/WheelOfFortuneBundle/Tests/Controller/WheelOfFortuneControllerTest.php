<?php
namespace TestDavid\WheelOfFortuneBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class WheelOfFortuneControllerTest extends WebTestCase {
    public static $shared_session = array(  ); 

    public function testIndex() {
        $client = static::createClient();

        $indexpage = $client->request('GET', '/');
        $this->assertEquals(
                1, $indexpage->filter('html:contains("index.intro")')->count(), 'geen introtekst aanwezig'
        );

        $this->assertEquals(
                1, $indexpage->selectButton('register_player_submit')->count(), 'geen submitbutton aanwezig'
        );
        $this->assertEquals(
                0, $indexpage->filter('html:contains("index.forminvalid")')->count(), 'melding invalid formulier onterecht aanwezig'
        );
    }

    public function testIndexFormZonderInputIsInvalid() {
        $client = static::createClient();
        $client->followRedirects();

        $indexpage = $client->request('GET', '/');
        $form = $indexpage->selectButton('register_player_submit')->form();
        $invalidindexpage = $client->submit($form);

        //Nog steeds op index-pagina
        $this->assertEquals(
                1, $invalidindexpage->filter('html:contains("index.intro")')->count(), 'geen introtekst aanwezig'
        );
        //Melding formulier invalid
        $this->assertEquals(
                1, $invalidindexpage->filter('html:contains("index.forminvalid")')->count(), 'geen melding invalid formulier aanwezig'
        );
        //7 Meldingen leeg veld
        $this->assertEquals(
                7, $invalidindexpage->filter('span.help-block:contains("This value should not be blank")')->count(), 'geen 7 meldingen leeg veld aanwezig'
        );
    }

    /**
     * @dataProvider getInvalidPlayers
     */
    public function testIndexFormMetFoutieveInputIsInvalid($player) {
        $client = static::createClient();
        $client->followRedirects();

        $indexpage = $client->request('GET', '/');
        $form = $indexpage->selectButton('register_player_submit')->form();
        $invalidindexpage = $client->submit($form, [ 'register_player' => $player]);

        //Specifieke foutmeldingen
        $this->assertEquals(
                1, $invalidindexpage->filter('html:contains("invalid.email")')->count(), 'geen melding fout mailadres aanwezig'
        );
        $this->assertEquals(
                1, $invalidindexpage->filter('html:contains("invalid.address")')->count(), 'geen melding fout adres aanwezig'
        );
        $this->assertEquals(
                1, $invalidindexpage->filter('html:contains("invalid.postcode")')->count(), 'geen melding fout postcode aanwezig'
        );
        $this->assertEquals(
                1, $invalidindexpage->filter('html:contains("invalid.phoneNumber")')->count(), 'geen melding fout telefoonnummer aanwezig'
        );
    }

    /**
     * @dataProvider getValidPlayers
     */
    public function testCorrecteFlow($player) {
        $client = static::createClient();
        $client->followRedirects();

        $indexpage = $client->request('GET', '/');
        $form = $indexpage->selectButton('register_player_submit')->form();

        $playpage = $client->submit($form, [ 'register_player' => $player]);
        if($playpage->filter('a:contains("play.spin")')->count() == 0){
            var_dump($playpage->filter('form')->text());
        }
        $this->assertEquals(
                1, $playpage->filter('a:contains("play.spin")')->count(), 'geen spin-button aanwezig'
        );
        $spinlink = $playpage->filter('a:contains("play.spin")')->first()->link();

        $spinpage = $client->click($spinlink);
        $this->assertEquals(
                0, $spinpage->filter('html:contains("play.spin")')->count(), 'wel spin-button aanwezig'
        );
        $result = $spinpage->filter('.spinresult')->text();
        $this->assertNotNull($result);

        for ($reload = 10; $reload--;) {
            $reloadpage = $client->request('GET', $spinlink->getUri());
            $this->assertEquals(
                    $result, $reloadpage->filter('.spinresult')->text(), 'resultaat is veranderd'
            );
        }
    }
    
    /**
     * @dataProvider getValidPlayers
     */
    public function testTweeKeerGaatNiet($player) {
        $client = static::createClient();
        $client->followRedirects();

        $indexpage = $client->request('GET', '/');
        $form = $indexpage->selectButton('register_player_submit')->form();

        $invalidindex = $client->submit($form, [ 'register_player' => $player]);
        $this->assertEquals(
                1, $invalidindex->filter('html:contains("onceper.email")')->count(), 'geen dubbele mail melding aanwezig'
        );
        $this->assertEquals(
                1, $invalidindex->filter('html:contains("onceper.phoneNumber")')->count(), 'geen dubbele telefoon melding aanwezig'
        );

    }

    public function testPlayZonderPlayerRedirectsNaarIndex() {
        $client = static::createClient();
        $client->followRedirects();

        $indexpage = $client->request('GET', '/play');

        $this->assertEquals(
                1, $indexpage->filter('html:contains("index.intro")')->count(), 'geen introtekst aanwezig'
        );
    }

    public function testSpinZonderPlayerGeeftFout() {
        $client = static::createClient();
        $client->followRedirects();

        $client->request('GET', '/spin');
        $this->assertFalse($client->getResponse()->isOK(), 'geen foutmelding');
    }
    
    /**
     * Test-data verwijderen
     */
    public static function tearDownAfterClass() {
        self::bootKernel();
        $em = static::$kernel->getContainer()
                ->get('doctrine')
                ->getManager()
        ;
        $connection = $em->getConnection();
        $platform = $connection->getDatabasePlatform();
        $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 0;');
        $connection->executeUpdate($platform->getTruncateTableSQL('wof_player'));
        $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 1;');
    }

    public function getValidPlayers() {
        return array(
            [ 'data' => [
                    'firstName' => 'David',
                    'lastName' => 'Suys',
                    'email' => 'dsuys@telenet.be',
                    'address' => 'Van Langenhovestraat 226 bus 2',
                    'postcode' => 'B-9200',
                    'city' => 'Sint-Gillis-bij-Dendermonde',
                    'phoneNumber' => '0495789696'
                ]
            ], [
                'data' => [
                    'firstName' => 'David',
                    'lastName' => 'Suys',
                    'email' => 'suys.david@gmail.com',
                    'address' => 'Stationsstraat 55',
                    'postcode' => 'B9255',
                    'city' => 'Buggenhout',
                    'phoneNumber' => '052341028',
                ]
            ]
        );
    }

    public function getInvalidPlayers() {
        return array(
            [ 'data' => [
                    'firstName' => 'test',
                    'lastName' => 'test',
                    'email' => 'test@test', //geen geldig mailadres
                    'address' => 'kerkstraat1', //geen geldig straatadress
                    'postcode' => '123', //geen geldige Belgische postcode
                    'city' => 'test',
                    'phoneNumber' => '911', //geen geldig Belgisch telefoonnummer
                ]
            ]
        );
    }

}
