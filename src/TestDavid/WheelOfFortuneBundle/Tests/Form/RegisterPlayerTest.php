<?php

namespace TestDavid\WheelOfFortuneBundle\Tests\Form;

use TestDavid\WheelOfFortuneBundle\Form\RegisterPlayer;
use TestDavid\WheelOfFortuneBundle\Entity\Player;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Form\PreloadedExtension;

class RegisterPlayerTest extends TypeTestCase {

    protected function getExtensions() {
        $childType = new PhoneNumberType();
        return array(new PreloadedExtension(array(
                $childType->getName() => $childType,
                    ), array()));
    }

    /**
     * @dataProvider getValidTestData
     */
    public function testSubmitValidData($formData) {
        $type = new RegisterPlayer();
        $form = $this->factory->create($type);

        // submit the data to the form directly
        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());
        $this->assertTrue($form->isValid());

        $view = $form->createView();
        $children = $view->children;
        
        $player = new Player();

        foreach ($formData as $key => $value) {
            $this->assertArrayHasKey($key, $children);
            
            // create a setter
            $setmethod = sprintf('set%s', ucwords($key));
            // use the method as a variable variable to set your value
            $player->$setmethod($value);
            
            // create a getter
            $getmethod = sprintf('get%s', ucwords($key));
            // use the method as a variable variable to get your value
            $this->assertEquals($value, $player->$getmethod());
        }
    }

    public function getValidTestData() {
        return [
            [
                "data" => [
                    'firstName' => 'First N.',
                    'lastName' => 'Ame',
                    'email' => 'test@test.com',
                    'address' => 'Wetstraat 16',
                    'postcode' => '1000',
                    'city' => 'Brussel',
                    'phoneNumber' => '021234567',
                ],
            ], [
                "data" => [
                    'firstName' => 'David',
                    'lastName' => 'Suys',
                    'email' => 'dsuys@telenet.be',
                    'address' => 'Van Langenhovestraat 226 bus 2',
                    'postcode' => 'B-9200',
                    'city' => 'Sint-Gillis-bij-Dendermonde',
                    'phoneNumber' => '0495789696',
                ],
            ], [
                "data" => [
                    'firstName' => 'David',
                    'lastName' => 'Suys',
                    'email' => 'suys.david@gmail.com',
                    'address' => 'Stationsstraat 55',
                    'postcode' => 'B9255',
                    'city' => 'Buggenhout',
                    'phoneNumber' => '052341028',
                ],
            ]
        ];
    }

}
