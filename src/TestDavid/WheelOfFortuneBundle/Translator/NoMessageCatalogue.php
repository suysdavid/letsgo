<?php
/**
 * Wordt gebruikt door NoTranslator
 */
namespace TestDavid\WheelOfFortuneBundle\Translator;

use Symfony\Component\Translation\MessageCatalogueInterface;

class NoMessageCatalogue implements MessageCatalogueInterface {
    public function add($messages, $domain = 'messages') {
        
    }

    public function addCatalogue(MessageCatalogueInterface $catalogue) {
        
    }

    public function addFallbackCatalogue(MessageCatalogueInterface $catalogue) {
        
    }

    public function addResource(\Symfony\Component\Config\Resource\ResourceInterface $resource) {
        
    }

    public function all($domain = null) {
        
    }

    public function defines($id, $domain = 'messages') {
        return true;
    }

    public function get($id, $domain = 'messages') {
        
    }

    public function getDomains() {
        
    }

    public function getFallbackCatalogue() {
        
    }

    public function getLocale() {
        
    }

    public function getResources() {
        
    }

    public function has($id, $domain = 'messages') {

    }

    public function replace($messages, $domain = 'messages') {
        
    }

    public function set($id, $translation, $domain = 'messages') {
        
    }

}
