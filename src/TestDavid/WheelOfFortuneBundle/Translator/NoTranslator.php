<?php
/**
 * NoTranslator, laat toe om 'taal-onafhankelijk' pagina's te testen
 * Bron: https://florian.ec/articles/use-translation-keys-in-symfony2-functional-tests/
 */
namespace TestDavid\WheelOfFortuneBundle\Translator;

use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Translation\TranslatorBagInterface;

class NoTranslator implements TranslatorInterface, TranslatorBagInterface {

    public function trans($id, array $parameters = array(), $domain = null, $locale = null) {
        return $id;
    }

    public function transChoice($id, $number, array $parameters = array(), $domain = null, $locale = null) {
        return $id;
    }

    public function setLocale($locale) {
        
    }

    public function getLocale() {
        return '--';
    }

    public function setFallbackLocales($locales) {
        
    }

    public function setFallbackLocale($locale) {
        
    }

    public function addResource($resource) {
        
    }

    public function getCatalogue($locale = null) {
        return new NoMessageCatalogue();
    }

}
